### Gender Classification from facial image
#### Gyalpozhing College of Information Technology (Bachelor of Science in Information Technology)
#### Prj303 (3rd year)

Gender recognition from face images is one of the fundamental re-search areas in computer vision. Automated gender recognition is important in many application areas such as human computer inter-action, biometric, surveillance, demographic statistics etc.

Check out the app [here](http://gogender.herokuapp.com/)!

Built using: HTML/CSS/JS, JupyterNotebook, Python, Bootstrap, CNN model

### Images and videos

Showcase video: https://youtu.be/WYt_1m0KxwI

Screenshot of the application: 

![cover page](/images/Screenshot2.png)

![image](/images/Screenshot7.png)

![result1](/images/Screenshot8.png)

![result2](/images/Screenshot9.png)

![result3](/images/Screenshot10.png)

### Final poster:

![poster](/images/poster.jpeg)

